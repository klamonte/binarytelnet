import java.io.InputStream;
import java.net.URL;

public class StarWarsViewer {

    public static void main(String [] args) throws Exception {

        // Register telnet as the standard handler for telnet protocol URLs.
        com.gitlab.klamonte.binarytelnet.TelnetURLConnection.register();

        // Connect to Star Wars ASCII telnet server.
        URL url = new URL("telnet://towel.blinkenlights.nl");
        InputStream input = url.openStream();

        // Display whatever we get from the server directly to stdout.
        for (int ch = input.read(); ch != -1; ch = input.read()) {
            System.out.printf("%c", (char) (ch & 0x7F));
            System.out.flush();
        }
        input.close();
    }

}
