/*
 * BinaryTelnet - Java Telnet Library
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package com.gitlab.klamonte.binarytelnet;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * This class provides a Socket that performs the telnet protocol to both
 * establish an 8-bit clean no echo channel and expose terminal window resize
 * events.  To set username, language, and terminal type, use the telescope
 * constructor methods.
 */
public class TelnetSocket extends Socket {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // Telnet protocol special characters.  Note package private access.
    static final int TELNET_SE         = 240;
    static final int TELNET_NOP        = 241;
    static final int TELNET_DM         = 242;
    static final int TELNET_BRK        = 243;
    static final int TELNET_IP         = 244;
    static final int TELNET_AO         = 245;
    static final int TELNET_AYT        = 246;
    static final int TELNET_EC         = 247;
    static final int TELNET_EL         = 248;
    static final int TELNET_GA         = 249;
    static final int TELNET_SB         = 250;
    static final int TELNET_WILL       = 251;
    static final int TELNET_WONT       = 252;
    static final int TELNET_DO         = 253;
    static final int TELNET_DONT       = 254;
    static final int TELNET_IAC        = 255;
    static final int C_NUL             = 0x00;
    static final int C_LF              = 0x0A;
    static final int C_CR              = 0x0D;

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The telnet-aware socket InputStream.
     */
    private TelnetInputStream input;

    /**
     * The telnet-aware socket OutputStream.
     */
    private TelnetOutputStream output;


    /**
     * If true, this is a server socket (i.e. created by accept()).
     */
    boolean isServer = true;

    /**
     * If true, telnet ECHO mode is set such that local echo is off and
     * remote echo is on.  This is appropriate for server sockets.
     */
    boolean echoMode = false;

    /**
     * If true, telnet BINARY mode is enabled.  We always want this to
     * ensure a Unicode-safe stream.
     */
    boolean binaryMode = false;

    /**
     * If true, the SUPPRESS-GO-AHEAD option is enabled.  We always want
     * this.
     */
    boolean goAhead = true;

    /**
     * If true, request the client terminal type.
     */
    boolean doTermType = true;

    /**
     * If true, request the client terminal speed.
     */
    boolean doTermSpeed = true;

    /**
     * If true, request the Negotiate About Window Size option to
     * determine the client text width/height.
     */
    boolean doNAWS = true;

    /**
     * If true, request the New Environment option to obtain the client
     * LOGNAME, USER, and LANG variables.
     */
    boolean doEnvironment = true;

    /**
     * The terminal type to send when a client, or reported by the client to
     * the server.
     */
    String terminalType = "xterm";

    /**
     * The terminal speed to send when a client, or reported by the client to
     * the server.
     */
    String terminalSpeed = "38400,38400";

    /**
     * User name.  This will be passed into the telnet USER and LOGNAME
     * environment variables.
     */
    String username = "";

    /**
     * Language.  This will be passed into the telnet LANG environment
     * variables.
     */
    String language = "";

    /**
     * Terminal window width.
     */
    int windowWidth = 80;

    /**
     * Terminal window height.
     */
    int windowHeight = 24;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Creates a Socket that knows the telnet protocol.  Note package private
     * access, this is only used by TelnetServerSocket.
     *
     * @throws IOException if an I/O error occurs
     */
    TelnetSocket() throws IOException {
        super();
    }

    /**
     * Creates a Socket that knows the telnet protocol.
     *
     * @param host host to connect to
     * @throws IOException if an I/O error occurs
     */
    public TelnetSocket(final String host) throws IOException {
        this(host, 23);
    }

    /**
     * Creates a Socket that knows the telnet protocol.
     *
     * @param host host to connect to
     * @param port port to connect to
     * @throws IOException if an I/O error occurs
     */
    public TelnetSocket(final String host, final int port) throws IOException {
        super(host, port);
    }

    /**
     * Telescope constructor menthod to set the username to send in the
     * telnet USER and LOGNAME environment variables.  Requires New
     * Environment option (RFC 1572) support.
     *
     * @param username the username
     * @return the telnet socket
     */
    public TelnetSocket username(final String username) {
        this.username = username;
        return this;
    }

    /**
     * Telescope constructor menthod to set the language to send in the
     * telnet LANG environment variable.  Requires New Environment option
     * (RFC 1572) support.
     *
     * @param language the language
     * @return the telnet socket
     */
    public TelnetSocket language(final String language) {
        this.language = language;
        return this;
    }

    /**
     * Telescope constructor menthod to set the terminal type to send using
     * the Terminal Type option (RFC 1091).  This also sets the telnet TERM
     * environment variable via the New Environment option (RFC 1572).
     *
     * @param terminalType the terminal type
     * @return the telnet socket
     */
    public TelnetSocket terminalType(final String terminalType) {
        this.terminalType = terminalType;
        return this;
    }

    // ------------------------------------------------------------------------
    // Socket -----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Returns an input stream for this socket.
     *
     * @return the input stream
     * @throws IOException if an I/O error occurs
     */
    @Override
    public InputStream getInputStream() throws IOException {
        if (input == null) {
            assert (output == null);
            output = new TelnetOutputStream(this, super.getOutputStream());
            input = new TelnetInputStream(this, super.getInputStream(), output);
            input.telnetSendOptions();
        }
        return input;
    }

    /**
     * Returns an output stream for this socket.
     *
     * @return the output stream
     * @throws IOException if an I/O error occurs
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
        if (output == null) {
            assert (input == null);
            output = new TelnetOutputStream(this, super.getOutputStream());
            input = new TelnetInputStream(this, super.getInputStream(), output);
            input.telnetSendOptions();
        }
        return output;
    }

    // ------------------------------------------------------------------------
    // TelnetSocket -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * See if telnet server/client is in ASCII mode.
     *
     * @return if true, this connection is in ASCII mode
     */
    public boolean isAscii() {
        return (!binaryMode);
    }

    /**
     * Get the username as reported by the telnet USER or LOGNAME environment
     * variables.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the language as reported by the telnet LANG environment variable.
     *
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Get the terminal type as reported by the telnet Terminal Type option.
     *
     * @return the terminal type
     */
    public String getTerminalType() {
        return terminalType;
    }

    /**
     * Get the terminal window width.
     *
     * @return the window width
     */
    public int getWindowWidth() {
        return windowWidth;
    }

    /**
     * Set terminal window width.  If the telnet NAWS option (RFC 1073) is
     * supported, this value will be sent to the remote side.
     *
     * @param windowWidth the window width
     * @throws IOException if an I/O error occurs
     */
    public void setWindowWidth(final int windowWidth) throws IOException {
        this.windowWidth = windowWidth;
        output.sendWindowSize();
    }

    /**
     * Get the terminal window height.
     *
     * @return the window height
     */
    public int getWindowHeight() {
        return windowHeight;
    }

    /**
     * Set terminal window height.  If the telnet NAWS option (RFC 1073) is
     * supported, this value will be sent to the remote side.
     *
     * @param windowHeight the window height
     * @throws IOException if an I/O error occurs
     */
    public void setWindowHeight(final int windowHeight) throws IOException {
        this.windowHeight = windowHeight;
        output.sendWindowSize();
    }

    /**
     * Set terminal window dimensions.  If the telnet NAWS option (RFC 1073) is
     * supported, this value will be sent to the remote side.
     *
     * @param windowWidth the window width
     * @param windowHeight the window height
     * @throws IOException if an I/O error occurs
     */
    public void setWindowHeight(final int windowWidth,
        final int windowHeight) throws IOException {

        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        output.sendWindowSize();
    }

}
