/*
 * BinaryTelnet - Java Telnet Library
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package com.gitlab.klamonte.binarytelnet;

import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLConnection;

/**
 * A stream protocol handler that knows how to make a connection for the
 * "telnet" protocol.
 */
public class TelnetURLStreamHandler extends URLStreamHandler {

    /**
     * Opens a connection to the object referenced by the URL argument.
     *
     * @param u the URL
     */
    @Override
    public URLConnection openConnection(URL u) {
        return new TelnetURLConnection(u);
    }

    /**
     * Returns the default port for a URL parsed by this handler.
     *
     * @return port 23 for telnet
     */
    @Override
    protected int getDefaultPort() {
        return 23;
    }

}
